<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Visit Date (Required)_glyphicon glyphi_cada34</name>
   <tag></tag>
   <elementGuidId>ec1c0a20-acec-43fc-8361-7f2f7f4e05cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[4]/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.glyphicon.glyphicon-calendar</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>881e3838-a280-47cd-99c4-6da46e94f553</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-calendar</value>
      <webElementGuid>14da20e3-fece-47d4-960e-480cede05de1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-4&quot;]/div[@class=&quot;input-group date&quot;]/div[@class=&quot;input-group-addon&quot;]/span[@class=&quot;glyphicon glyphicon-calendar&quot;]</value>
      <webElementGuid>b0b38795-1aa3-4644-90a6-840ed15f5bc0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]/div/div/div/span</value>
      <webElementGuid>38ce8b76-bf26-417a-9083-f3211ea97d6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>fffd97fc-e5cc-435a-a66b-130e2ebe7f3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
